CREATE database libreria;
USE libreria;
create TABLE libros(
id int auto_increment primary key,
isbn varchar(20) not null UNIQUE,
titulo varchar(50) not null,
auto varchar(50),
fecha_publicacion timestamp
);