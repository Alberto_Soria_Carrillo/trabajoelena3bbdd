package libreriaMVC;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Modelo {

    private Connection conexion;

    public void conectar() throws SQLException {
        conexion = DriverManager.getConnection("jdbc:mysql://http:localhost/3306/libreria", "root", "");
    }

    public void desconectar() throws SQLException {
        conexion.close();
    }

    public void insertar(String isbn, String titulo, String autor,  LocalDateTime fecha_publicacion) throws SQLException {
        String consulta= "INSERT INTO libros(isbn, titulo, autor, fecha_publicacion) VALUE (?,?,?,?)";
        PreparedStatement statement = null;
        statement = conexion.prepareStatement(consulta);

        statement.setString(1,isbn);
        statement.setString(2,titulo);
        statement.setString(3, autor);
        statement.setTimestamp(4, Timestamp.valueOf(fecha_publicacion));
        statement.execute();
        statement.close();

    }

    public void eliminar(String isbn) throws SQLException {

        String eliminar = "DELETE FROM libros WHERE isbn=?";
        PreparedStatement statement = null;

        statement = conexion.prepareStatement(eliminar);

        statement.setString(1,isbn);

        statement.executeUpdate();

        if(statement==null){

            statement.close();

        }



    }

    public ResultSet consultaLibros() throws SQLException {

        String consulta = "SELECT * FROM libros";

        PreparedStatement statement = null;

        ResultSet resultSet = null;

        statement = conexion.prepareStatement(consulta);

        resultSet = statement.getResultSet();

        return resultSet;

    }

}
