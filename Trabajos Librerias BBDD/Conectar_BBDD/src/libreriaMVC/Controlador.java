package libreriaMVC;

import main.Vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Controlador implements ActionListener {

    private Modelo modelo;
    private Vista vista;

    public Controlador(Vista vista, Modelo modelo) throws SQLException {
        this.modelo = modelo;
        this.vista = vista;
        anidarAcctionListeners(this);
        modelo.conectar();

        try {

            modelo.conectar();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    private void anidarAcctionListeners(ActionListener listener) {

        vista.btn_eliminar.addActionListener(listener);
        vista.btn_insertar.addActionListener(listener);
        vista.btn_listar.addActionListener(listener);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch (comando) {
            case "Insertar":

                try {
                    modelo.insertar(vista.txt_isb.getText(), vista.txt_titulo.getText(),
                            vista.txt_autor.getText(), vista.dateTimePicker.getDateTimePermissive());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                break;
            case "Listar":

                int num;

                try {
                    modelo.consultaLibros();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                break;
            case "Eliminar":

                try {
                    modelo.eliminar(vista.txt_isb.getText());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                break;
            default:
                break;
        }
    }
}
