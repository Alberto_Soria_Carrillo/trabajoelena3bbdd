package main;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    public JPanel panel1;
    public JTextField txt_isb;
    public JTextField txt_titulo;
    public JTextField txt_autor;
    public JButton btn_insertar;
    public JButton btn_eliminar;
    public JButton btn_listar;
    public JTable table1;
    public DateTimePicker dateTimePicker;

    // creadas
    public DefaultTableModel dtm;

    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(new Vista().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        iniciarTabla();
    }

    private void iniciarTabla(){

        dtm = new DefaultTableModel();
        table1.setModel(dtm);

        Object[] cabeceras = {"id", "isbn", "autor", "fecha_publicacion"};
        dtm.setColumnIdentifiers(cabeceras);

    }

}
